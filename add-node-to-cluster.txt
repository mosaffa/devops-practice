# This guildline is required when the initial token is expired.

# on master:
# ----------
# First create new token
$ kubeadm token create

# Then retrieve hash 
$ openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed ‘s/^.* //’


# on new node:
# ------------
$ sudo kubeadm join 172.31.117.79:6443 --token <copy token from first command>  --discovery-token-ca-cert-hash sha256:<copy hash from second command>